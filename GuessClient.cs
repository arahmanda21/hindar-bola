﻿/*
 * Created by SharpDevelop.
 * User: sully
 * Date: 14/05/2019
 * Time: 12:34
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace Testing_Client
{
class Program {
	static void Main(string[] args) {
	string input = " ", ServerIp;
   	bool Multiclient = false, Error = false;

    do {
   		Console.WriteLine("Guess The Number");
    	Console.WriteLine("1. Play Alone");
    	Console.WriteLine("2. Multiplayer");
    	Console.WriteLine("3. How to play");
    	Console.WriteLine("4. Exit");
    	Console.Write("Answer : ");
    	input = Console.ReadLine();
    
    	switch(input) {
    	  	case "1":
    			Playgame();
    	  		break;
    	  	case "2":
   				Multiclient = true;
   			
   				Console.Write("Enter Server's IP Address : ");
   				ServerIp = Console.ReadLine();
   				
   				IPAddress ip = IPAddress.Parse(ServerIp);
        		int port = 5000;
        		bool clientClosed = false;
        		TcpClient client = new TcpClient();
        		client.Connect(ip, port);
        		NetworkStream ns = client.GetStream();
        		Thread thread = new Thread(o => ReceiveData((TcpClient)o));

        		thread.Start(client);
        		Console.Write("Please input your Username : ");
        		string s;
        		while (!string.IsNullOrEmpty((s = Console.ReadLine())))
        		{
            		byte[] buffer = Encoding.ASCII.GetBytes(s);
            		ns.Write(buffer, 0, buffer.Length);
            		
            		if(s == "quit" || s == "Quit")
						break;            			
        		}

        		client.Client.Shutdown(SocketShutdown.Send);
        		thread.Join();
        		ns.Close();
        		client.Close();
        		Console.WriteLine("disconnect from server!!");
        		Console.ReadKey();
    	  	break;
      	case "3":
      		HowToPlay();
      		break;
      	default:
      		break;
    	}
   	
   	}while(input != "4");

      
}
	static void ReceiveData(TcpClient client)
    {
        NetworkStream ns = client.GetStream();
        byte[] receivedBytes = new byte[1024];
        int byte_count;

        while ((byte_count = ns.Read(receivedBytes, 0, receivedBytes.Length)) > 0)
        {
            Console.Write(Encoding.ASCII.GetString(receivedBytes, 0, byte_count));
        }
    }
	public static void Playgame() {
   		string input = " ";
   		
   		int i, j, CorrectPosition = 0, WrongPosition = 0, limit;
   		int[] n, x;
   		n = new int[4]; //deklarasi variabel array u/ jawaban
   		x = new int[4]; //deklarasi variabel array u/ inputan
   		Random random = new Random();
   		
   		Console.WriteLine("Game Started");
   		Console.WriteLine("");
   		Console.WriteLine("Hint!");
   		Console.WriteLine("B : Correct number, correct position");
   		Console.WriteLine("S : Correct number, wrong position");
   		
   		//menerima jawaban angka acak dari server
   		for(i = 0; i < 4; i++) {
            	for (i = 0; i < 4; i++) {
            	    n[i] = random.Next(0, 9);
	
            		for (j = i; j > 0; j--) {
            		    if (j == 1){
            		    	while (n[i] == n[j - 1])
            		    	n[i] = random.Next(0, 9);
            		    } else if (j == 2) {
            		    	while (n[i] == n[j - 1] || n[i] == n[j - 2])
            		    	n[i] = random.Next(0, 9);
            		    } else if (j == 3) {
            		    	while (n[i] == n[j - 1] || n[i] == n[j - 2] || n[i] == n[j - 3])
            		    	n[i] = random.Next(0, 9);
            		    }
            		}
   				}
   		}
   		
   		Console.WriteLine("");
   		limit = 0;
   		
   		//membuat perulangan untuk inputan jawaban
   		do {
   			switch(limit+1) {
      			case 1:
      				Console.Write("1st Attempt");
      				break;
      			case 2:
      				Console.Write("2nd Attempts");
      				break;
      			case 3:
      				Console.Write("3rd Attempts");
      				break;
      			default:
      				Console.Write(limit+1 +"th Attempts");
      				break;
      			}
   			
   			//mencetak berapa jumlah 'angka benar letak benar' dan 'angka benar letak salah' pemain
   			Console.WriteLine(" - B" + CorrectPosition + " S" + WrongPosition);
   			CorrectPosition = 0; //reset 'jumlah angka benar letak benar'
   			WrongPosition = 0; //reset 'jumlah angka benar letak salah'
   			
   			//perintah input jawaban untuk pemain
   			input = Console.ReadLine();
   			string[] answers = input.Split(' ');
   			
   			i = 0;
   			foreach (string answer in answers) {
   				x[i] = int.Parse(answer);
   				i++;
   			}
   			
   			//mengecek jumlah angka benar dan letak benar
   			for(i=0; i<4; i++) {
   				if(x[i] == n[i])
   					CorrectPosition++; 
   			}
   			
   			//mengecek jumlah angka benar, tapi letak salah
   			for(i=0; i<4; i++) {
   				for(j = 0; j < 4; j++) {
   					if(x[i] == n[j])
   						WrongPosition++;
   				}
   			}
   			WrongPosition = WrongPosition - CorrectPosition;
   			
   			Console.WriteLine("");
   			limit++;
   		} while(CorrectPosition != 4 && limit < 8); //pemain mendapat 8 kali percobaan menjawab
   		
   		if(CorrectPosition == 4) {
   			Console.WriteLine("YOU WON!"); //bila pemain berhasil menebak
   		} else {
   			Console.WriteLine("YOU LOSE!"); //bila pemain gagal menebak
   		}
   		
   		//mencetak kunci jawaban
   		Console.Write("Answer : ");
   		for(i = 0; i < 4; i++) {
   			Console.Write(n[i] + " ");
   		}
   		Console.WriteLine("");
   		   
		//mengakhiri permmainan   		
   		Console.WriteLine("");
   		Console.WriteLine("Game Ended");
   		Console.WriteLine("");
   	  }   
   	public static void HowToPlay() {
   		Console.WriteLine("== Cara Bermain ==");
   		Console.WriteLine("1. Akan diacakkan 4 angka yang berbeda, anda harus bisa menebak ke-4 angka tersebut dengan peletakan yang benar");
   		Console.WriteLine("2. Contoh bila jawabannya \"4 2 1 7\", anda harus menjawab dengan \"4 2 1 7\" juga");
   		Console.WriteLine("3. Bila anda menjawabnya dengan peletakan yang berbeda seperti \"1 4 7 2\", itu salah");
   		Console.WriteLine("4. Untuk mempermudah, anda akan diberi hint berupa \"B & S\"");
   		Console.WriteLine("5. B merupakan jumlah angka tebakan yang benar dan letaknya juga benar");
   		Console.WriteLine("6. S merupakan jumlah angka tebakan yang benar tapi letaknya salah");
   		Console.WriteLine("== Selamat bermain ==");
   		Console.WriteLine("");
	}
}	
}
