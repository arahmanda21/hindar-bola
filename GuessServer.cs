/*
 * Created by SharpDevelop.
 * User: sully
 * Date: 14/05/2019
 * Time: 12:32
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace Testing_Server
{
	public static class gvar{
		public static int playerCount = 0;
        public static string [] playerList={"",""};
        public static bool [] playerReady = {false, false};
        public static bool [] playerStatus = {false, false};
        public static int [] n = {0, 0, 0, 0}, x = {0, 0, 0, 0};
        public static bool [] gameStarted = {false, false};
        public static bool gameEnded = false;
        public static bool draw = false;
	}
	class Program
{
    static readonly object _lock = new object();
    static readonly Dictionary<int, TcpClient> list_clients = new Dictionary<int, TcpClient>();

    static void Main(string[] args)
    {
    	Console.WriteLine("[Server]");
    	
    	
        TcpListener ServerSocket = new TcpListener(IPAddress.Any, 5000);
        ServerSocket.Start();
         
		while(true) {        
        while (gvar.playerCount != 1)
        {
            TcpClient client = ServerSocket.AcceptTcpClient();
            lock (_lock) list_clients.Add(gvar.playerCount, client);

            Thread t = new Thread(handle_clients);
            t.Start(gvar.playerCount);
        }
        
        while (gvar.playerReady[0] == false || gvar.playerReady[1] == false)
        {

        }
		Console.WriteLine("[Semua pemain telah siap, permainan dimulai]");        
   		Random random = new Random();
   		int i, j;
   		
   		//menerima jawaban angka acak dari server
   		for(i = 0; i < 4; i++) {
            	for (i = 0; i < 4; i++) {
            	    gvar.n[i] = random.Next(0, 9);
	
            		for (j = i; j > 0; j--) {
            		    if (j == 1){
            		    	while (gvar.n[i] == gvar.n[j - 1])
            		    	gvar.n[i] = random.Next(0, 9);
            		    } else if (j == 2) {
            		    	while (gvar.n[i] == gvar.n[j - 1] || gvar.n[i] == gvar.n[j - 2])
            		    	gvar.n[i] = random.Next(0, 9);
            		    } else if (j == 3) {
            		    	while (gvar.n[i] == gvar.n[j - 1] || gvar.n[i] == gvar.n[j - 2] || gvar.n[i] == gvar.n[j - 3])
            		    	gvar.n[i] = random.Next(0, 9);
            		    }
            		}
   				}
   		}
   		
   		do {
   			
   		}while(gvar.gameEnded == false);
   		
   		if(gvar.draw == true){
   			Console.WriteLine("[Tak ada pemenang dalam game]");
   			gvar.draw = false;
    	}
   		
        }
    }

    public static void handle_clients(object o)
    {
        int id = (int)o;
        
        TcpClient client;
        lock (_lock) client = list_clients[id];
		NetworkStream stream = client.GetStream();
        byte[] buffer = new byte[1024];
        
        int byte_count = stream.Read(buffer, 0, buffer.Length);
        string data = Encoding.ASCII.GetString(buffer, 0, byte_count);
        string username = data;
        
        if (gvar.playerCount == 0)
        	Console.WriteLine("\n[Akan memulai sebuah permainan]");
        
        gvar.playerList[gvar.playerCount] = username;
        Console.WriteLine("[" + gvar.playerList[gvar.playerCount] + " masuk ke dalam permainan]");
        
        if (gvar.playerCount < 1) {
        	buffer = Encoding.ASCII.GetBytes("\nMenunggu pemain lain masuk.... ");
        	stream.Write(buffer, 0, buffer.Length); 
        }
        gvar.playerCount++;
        
        do{
       	//Saat semua pemain sudah masuk
        } while (gvar.playerCount != 2);
        
        do{
        if(gvar.playerReady[id] == true)
       		buffer = Encoding.ASCII.GetBytes("Menunggu pemain lain siap...\n");
        else
        	buffer = Encoding.ASCII.GetBytes("\nSemua pemain sudah masuk, apa kamu sudah siap? (Y/N) ");
        stream.Write(buffer, 0, buffer.Length);
        
        if(gvar.playerReady[id] == false) {
        	byte_count = stream.Read(buffer, 0, buffer.Length);
        	data = Encoding.ASCII.GetString(buffer, 0, byte_count);
        } else {
        	do {
        		
        	} while (gvar.playerReady[0] == false || gvar.playerReady[1] == false);
        }
        
        if (data == "Y" || data == "y") {
        		gvar.playerReady[id] = true;
        }
        } while (gvar.playerReady[0] == false || gvar.playerReady[1] == false);
       	
        gvar.gameStarted[0] = true;
        gvar.gameStarted[1] = true;
        
        buffer = Encoding.ASCII.GetBytes("\n[Game started]\n\nHint!\nB : Correct number, correct position\nS : Correct number, wrong position\n");
        stream.Write(buffer, 0, buffer.Length);
        
        int i, j, CorrectPosition = 0, WrongPosition = 0, limit = 0;
        //play the game        
        while (gvar.gameStarted[0] == true || gvar.gameStarted[1] == true) {
   		
   		//membuat perulangan untuk inputan jawaban
   		while(CorrectPosition != 4 && limit < 8) {
   			switch(limit+1) {
      			case 1:
   					buffer = Encoding.ASCII.GetBytes("\n1st Attempt");
        			stream.Write(buffer, 0, buffer.Length);
      				break;
      			case 2:
      				buffer = Encoding.ASCII.GetBytes("\n2nd Attemptz");
        			stream.Write(buffer, 0, buffer.Length);
      				break;
      			case 3:
      				buffer = Encoding.ASCII.GetBytes("\n3nd Attempts");
        			stream.Write(buffer, 0, buffer.Length);
      				break;
      			default:
      				buffer = Encoding.ASCII.GetBytes("\n" + (limit+1) +"th Attempts");
        			stream.Write(buffer, 0, buffer.Length);
      				break;
      			}
   			
   			//mencetak berapa jumlah 'angka benar letak benar' dan 'angka benar letak salah' pemain
   			buffer = Encoding.ASCII.GetBytes(" - B" + CorrectPosition + " S" + WrongPosition + "\n");
        	stream.Write(buffer, 0, buffer.Length);
   			CorrectPosition = 0; //reset 'jumlah angka benar letak benar'
   			WrongPosition = 0; //reset 'jumlah angka benar letak salah'
   			
   			//perintah input jawaban untuk pemain
   			byte_count = stream.Read(buffer, 0, buffer.Length);
        	data = Encoding.ASCII.GetString(buffer, 0, byte_count);
   			string[] answers = data.Split(' ');
   			
   			i = 0;
   			foreach (string answer in answers) {
   				gvar.x[i] = int.Parse(answer);
   				i++;
   			}
   			
   			//mengecek jumlah angka benar dan letak benar
   			for(i=0; i<4; i++) {
   				if(gvar.x[i] == gvar.n[i])
   					CorrectPosition++; 
   			}
   			
   			//mengecek jumlah angka benar, tapi letak salah
   			for(i=0; i<4; i++) {
   				for(j = 0; j < 4; j++) {
   					if(gvar.x[i] == gvar.n[j])
   						WrongPosition++;
   				}
   			}
   			WrongPosition = WrongPosition - CorrectPosition;
   			limit++;
   			
   			if(CorrectPosition == 4 && gvar.gameStarted[id] == true) {
				gvar.playerStatus[id] = true;
				gvar.gameStarted[0] = false;
				gvar.gameStarted[1] = false;
   			}
   			
   			if(limit == 8) {
   				gvar.gameStarted[id] = false;
   				buffer = Encoding.ASCII.GetBytes("\nMenunggu pemain lain menyelesaikan permainan");
        		stream.Write(buffer, 0, buffer.Length);
   			}
   		
   		} //pemain mendapat 8 kali percobaan menjawab
        
        }
   		
        if(gvar.playerStatus[id] == true) {
        	Console.WriteLine("[" + gvar.playerList[id] + " memenangkan permainan]");
   			buffer = Encoding.ASCII.GetBytes("\nYOU WON!\n");
        	stream.Write(buffer, 0, buffer.Length);
        } else if(gvar.playerStatus[0] == false && gvar.playerStatus[1] == false){
        	gvar.draw = true;
   			buffer = Encoding.ASCII.GetBytes("\nDRAW! No one win the game\n");
        	stream.Write(buffer, 0, buffer.Length);
        } else {
        	buffer = Encoding.ASCII.GetBytes("\nYOU LOSE!\n");
        	stream.Write(buffer, 0, buffer.Length);
        }
        
   		//mencetak kunci jawaban
   		buffer = Encoding.ASCII.GetBytes("\nAnswer : ");
        stream.Write(buffer, 0, buffer.Length);
   		for(i = 0; i < 4; i++) {
   			buffer = Encoding.ASCII.GetBytes(gvar.n[i] + " ");
   			stream.Write(buffer, 0, buffer.Length);
   		}
   		  
        buffer = Encoding.ASCII.GetBytes("\nGame Ended, write \"quit\" to exit this multiplayer : ");
   		stream.Write(buffer, 0, buffer.Length);
        
        lock (_lock) list_clients.Remove(id);
        client.Client.Shutdown(SocketShutdown.Both);
        client.Close();
        
        gvar.gameEnded = true;
        gvar.playerCount = 0;
        gvar.playerList[0] = "";
        gvar.playerList[1] = "";
        gvar.playerReady[0] = false;
        gvar.playerReady[1] = false;
        gvar.playerStatus[0] = false;
        gvar.playerStatus[1] = false;
        gvar.gameStarted[0] = false;
        gvar.gameStarted[1] = false;
    }

    public static void broadcast(string data)
    {
        byte[] buffer = Encoding.ASCII.GetBytes(data + Environment.NewLine);

        lock (_lock)
        {
            foreach (TcpClient c in list_clients.Values)
            {
                NetworkStream stream = c.GetStream();

                stream.Write(buffer, 0, buffer.Length);
            }
        }
    }  
}
}
